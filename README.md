A distributed app that calculates distance matrix.

how to run:
1. clone the project
2. install maven
3. type 'mvn spring-boot:run' in the command line (inside the project folder)
4. the app will be lunched on port 8080.
5. go to http://localhost:8080 in order to see an example of an api request.

api request example: <br>
URL - http://localhost:8080/DistanceMatrix/Calculate <br>
METHOD - POST <br>
BODY - <br>
{
  "ParallismLevel":10,
  "Points":[
    {
      "x":0,
      "y":0
    },
    {
      "x":1,
      "y":0
    },
    {
      "x":2,
      "y":0
    },
    {
      "x":3,
      "y":0
    },
    {
      "x":4,
      "y":0
    }
  ]
}