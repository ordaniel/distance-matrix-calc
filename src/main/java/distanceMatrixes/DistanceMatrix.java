package distanceMatrixes;

public abstract class DistanceMatrix {
    public abstract double get(int row,int column);
    public abstract void put(int row,int column, double value);
}
