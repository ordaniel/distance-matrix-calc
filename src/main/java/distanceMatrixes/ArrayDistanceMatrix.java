package distanceMatrixes;

import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArrayDistanceMatrix extends DistanceMatrix {

    double[][] distanceMatrix;

    public ArrayDistanceMatrix(int rowSize, int colSize) {
        this.distanceMatrix = new double[rowSize][colSize];

        for (int currRow = 0; currRow < rowSize; currRow++) {
            for (int currCol = 0; currCol < colSize; currCol++) {
                distanceMatrix[currRow][currCol] = 0.0;
            }
        }
    }

    @Override
    public double get(int row, int column) {
        return distanceMatrix[row][column];
    }

    @Override
    public void put(int row, int column, double value) {
        distanceMatrix[row][column] = value;
    }

    @Override
    public String toString() {
        StringBuffer strMatrix = new StringBuffer();
        String cellsDelimiter =",";
        String rowDelimiter = "\n";

        for(double row[] : distanceMatrix) {
            for(double cell : row) {
                strMatrix.append(String.valueOf(cell)).append(cellsDelimiter);
            }
            strMatrix.deleteCharAt(strMatrix.length() -1);
            strMatrix.append(rowDelimiter);
        }
        strMatrix.deleteCharAt(strMatrix.length() - 1);
        return strMatrix.toString();
    }
}
