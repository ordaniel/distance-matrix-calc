package distanceMatrixes.distanceMatrixFactories;

import distanceMatrixes.DistanceMatrix;

public interface DistanceMatrixFactory<T extends DistanceMatrix> {
    T createInstance(int rows, int cols);
}
