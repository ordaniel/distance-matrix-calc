package distanceMatrixes.distanceMatrixFactories;

import distanceMatrixes.ArrayDistanceMatrix;
import distanceMatrixes.DistanceMatrix;

public class ArrayDistanceMatrixFactory implements DistanceMatrixFactory<ArrayDistanceMatrix> {
    @Override
    public ArrayDistanceMatrix createInstance(int rows, int cols) {
        return new ArrayDistanceMatrix(rows,cols);
    }
}
