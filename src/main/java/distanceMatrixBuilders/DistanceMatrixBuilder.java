package distanceMatrixBuilders;

import distanceMatrixes.DistanceMatrix;

public interface DistanceMatrixBuilder {
    DistanceMatrix build() throws Exception;
}
