package distanceMatrixBuilders;

import distanceMatrixes.DistanceMatrix;
import distanceCalculators.DistanceCalculator;
import distanceMatrixes.distanceMatrixFactories.DistanceMatrixFactory;
import requestPOJOS.DistancesRequest;
import requestPOJOS.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class AsyncDistanceMatrixBuilder implements DistanceMatrixBuilder {

    private final List<Point> points;
    private final ExecutorService executorService;
    private final DistanceCalculator distanceCalculator;
    DistanceMatrix distanceMatrix;

    public AsyncDistanceMatrixBuilder(DistancesRequest distancesRequest,
                                      DistanceCalculator distanceCalculator,
                                      DistanceMatrixFactory distanceMatrixFactory) {
        this.executorService = Executors.newFixedThreadPool(distancesRequest.getPoolSize());
        this.points = distancesRequest.getPoints();
        this.distanceCalculator = distanceCalculator;
        int rowSize = distancesRequest.getPoints().size();
        int colSize = distancesRequest.getPoints().size();
        this.distanceMatrix = distanceMatrixFactory.createInstance(rowSize,colSize);
    }

    @Override
    public DistanceMatrix build() throws InterruptedException {

        int pointsSize = points.size();
        int numOfTasks = numOfAllPointPairsCombination(pointsSize);
        CountDownLatch countDownLatch = new CountDownLatch(numOfTasks);

        for (int currSrcPointIndex = 0; currSrcPointIndex < pointsSize; currSrcPointIndex++) {
            Point currSrcPoint = points.get(currSrcPointIndex);
            for (int currDestPointIndex = currSrcPointIndex+1; currDestPointIndex < pointsSize; currDestPointIndex++) {
                Point currDestPoint = points.get(currDestPointIndex);
                Location matrixLocation = new Location(currSrcPointIndex,currDestPointIndex);
                CompletableFuture.runAsync(() -> {
                    double distance = distanceCalculator.calcDistanceBetweenPoints(currSrcPoint, currDestPoint);
                    distanceMatrix.put(matrixLocation.X(),matrixLocation.Y(),distance);
                    distanceMatrix.put(matrixLocation.Y(),matrixLocation.X(),distance);
                    countDownLatch.countDown();
                }, this.executorService);
            }
        }

        countDownLatch.await();
        return distanceMatrix;
    }

    private int numOfAllPointPairsCombination(int pointsSize) {
        return ((pointsSize)*(pointsSize -1))/2;
    }

    private final class Location {
        private int x;
        private int y;

        public Location(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int X() {
            return x;
        }

        public int Y() {
            return y;
        }
    }
}
