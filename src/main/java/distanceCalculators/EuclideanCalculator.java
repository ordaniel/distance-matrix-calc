package distanceCalculators;

import requestPOJOS.Point;

public class EuclideanCalculator implements DistanceCalculator {

    public EuclideanCalculator() {
    }

    @Override
    public double calcDistanceBetweenPoints(Point srcPoint, Point destPoint) {
        double x1 = srcPoint.getX_coordinate();
        double y1 = srcPoint.getY_coordinate();
        double x2 = destPoint.getX_coordinate();
        double y2 = destPoint.getY_coordinate();
        double euclideanDistance = Math.sqrt(Math.pow(x1 - x2,2) + Math.pow(y1 - y2,2));
        return euclideanDistance;
    }
}
