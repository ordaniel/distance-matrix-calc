package distanceCalculators;

import requestPOJOS.Point;

public interface DistanceCalculator {
    double calcDistanceBetweenPoints(Point srcPoint, Point destPoint);
}
