package requestPOJOS;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DistancesRequest {

    @JsonProperty(value = "ParallismLevel", required = true)
    private int poolSize;

    @JsonProperty(value = "Points", required = true)
    private List<Point> points;

    @JsonCreator
    public DistancesRequest( int poolSize, List<Point> points) throws Exception {
        this.poolSize= poolSize;
        this.points = points;
        //this.poolSize = poolSize.orElseThrow(() -> new Exception("parallism level must be provided"));
        //this.points = points.orElseThrow(() -> new Exception(" points must be provided"));;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
