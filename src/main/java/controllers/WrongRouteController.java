package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import requestPOJOS.DistancesRequest;
import requestPOJOS.Point;

import java.util.ArrayList;
import java.util.List;

@RestController
public class WrongRouteController {

    @RequestMapping("/**")
    public ResponseEntity<String> wrongRoute() throws Exception {
        String message = "Welcome to distance-matrix-calculator! api request example: \n" +
                "Method - POST \n" +
                "Route - /DistanceMatrix/Calculate \n" +
                "Body - " + generateRequestExample();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    private String generateRequestExample() throws Exception {
        List<Point> points = new ArrayList<>();
        points.add(new Point(0,0));
        points.add(new Point(0,1));
        points.add(new Point(1,0));
        points.add(new Point(1,1));
        points.add(new Point(2,2));
        DistancesRequest requestExample = new DistancesRequest(10,points);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(requestExample);
    }
}