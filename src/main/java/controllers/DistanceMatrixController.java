package controllers;

import distanceCalculators.DistanceCalculator;
import distanceCalculators.EuclideanCalculator;
import distanceMatrixBuilders.AsyncDistanceMatrixBuilder;
import distanceMatrixBuilders.DistanceMatrixBuilder;
import distanceMatrixes.DistanceMatrix;
import distanceMatrixes.distanceMatrixFactories.ArrayDistanceMatrixFactory;
import distanceMatrixes.distanceMatrixFactories.DistanceMatrixFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import requestPOJOS.DistancesRequest;

@RestController
public class DistanceMatrixController {

    @PostMapping(value = "/DistanceMatrix/Calculate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> CalculateDistanceMatrix(@RequestBody DistancesRequest distancesRequest) throws Exception {

        DistanceCalculator distanceCalculator = new EuclideanCalculator();
        DistanceMatrixFactory distanceMatrixFactory = new ArrayDistanceMatrixFactory();
        DistanceMatrixBuilder distanceMatrixBuilder = new AsyncDistanceMatrixBuilder(distancesRequest,distanceCalculator,distanceMatrixFactory);
        DistanceMatrix distanceMatrix = distanceMatrixBuilder.build();
        return new ResponseEntity<String>(distanceMatrix.toString(),HttpStatus.OK);
    }
}
