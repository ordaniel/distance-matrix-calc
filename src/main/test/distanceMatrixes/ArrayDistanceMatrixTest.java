package distanceMatrixes;

import org.junit.Test;

import java.lang.reflect.Array;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ArrayDistanceMatrixTest {

    @Test
    public void toStringTest() {
        //Setup
        StringBuilder expectedValue = new StringBuilder();
        String rowDelimiter = "\n";
        expectedValue.append("0.0,0.0,0.0").append(rowDelimiter);
        expectedValue.append("0.0,0.0,0.0").append(rowDelimiter);
        expectedValue.append("0.0,0.0,0.0");

        //When
        ArrayDistanceMatrix distanceMatrix = new ArrayDistanceMatrix(3,3);

        //Then
        assertThat(distanceMatrix.toString(), equalTo(expectedValue.toString()));
    }
}