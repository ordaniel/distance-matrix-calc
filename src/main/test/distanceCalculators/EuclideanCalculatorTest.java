package distanceCalculators;

import org.junit.Test;
import requestPOJOS.Point;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class EuclideanCalculatorTest {

    @Test
    public void calcDistanceBetweenPoints() {
        // Setup
        Point src = new Point(2,-1);
        Point dest = new Point(-2,2);
        double expectedDistance = 5;
        DistanceCalculator distanceCalculator = new EuclideanCalculator();

        // When
        double actualSrcToDestDistance = distanceCalculator.calcDistanceBetweenPoints(src,dest);
        double actualDestToSecDistance = distanceCalculator.calcDistanceBetweenPoints(dest,src);

        // Then
        assertThat(actualSrcToDestDistance, equalTo(expectedDistance));
        assertThat(actualDestToSecDistance, equalTo(expectedDistance));
    }
}