package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import distanceMatrixes.ArrayDistanceMatrix;
import distanceMatrixes.DistanceMatrix;
import io.restassured.RestAssured;
import mainExecutor.Application;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import requestPOJOS.DistancesRequest;
import requestPOJOS.Point;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class,webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DistanceMatrixControllerIT {

    public DistanceMatrixControllerIT() {
        RestAssured.port = 8080;
    }

    @Test
    public void stressTest() throws Exception {
        // Setup
        int pointsSize = 5000;
        int poolSize = 30;
        JsonNode request = generateRequest(pointsSize,poolSize);
        String expectedResponse = generateExpectedResponse(pointsSize);

        given().
                header("Content-Type", "application/json").
                body(request).
        when().
                post("/DistanceMatrix/Calculate").
        then().
                statusCode(HttpStatus.SC_OK).
                body(equalTo(expectedResponse));
    }

    private String generateExpectedResponse(int pointsSize) {
        DistanceMatrix distanceMatrix = new ArrayDistanceMatrix(pointsSize,pointsSize);
        for (int row = 0; row < pointsSize; row++) {
            int currExpectedDistance = 1;
            for (int col = row+1; col < pointsSize; col++) {
                distanceMatrix.put(row,col,currExpectedDistance);
                distanceMatrix.put(col,row,currExpectedDistance);
                currExpectedDistance++;
            }
        }
        return distanceMatrix.toString();
    }

    private JsonNode generateRequest(int pointsSize, int poolSize) throws Exception {
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < pointsSize; i++) {
            points.add(new Point(0, i));
        }
        DistancesRequest requestExample = new DistancesRequest(poolSize,points);
        ObjectMapper objectMapper = new ObjectMapper();
        String strRequest = objectMapper.writeValueAsString(requestExample);
        JsonNode request = objectMapper.readTree(strRequest);
        return request;
    }
}